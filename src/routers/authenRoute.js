import React from 'react';
import { Redirect, Route } from 'react-router-dom';
// import { checkAuthentication } from '../../store/selectors/user';
const checkAuthentication = () => {
  return false
}
function withAuthRoute(checkAuth, pathname) {

  return function AuthRoute({ component: Component, ...rest }) {
    return (
      <Route
        {...rest}
        render={props => {
          if (checkAuth()) {
            return <Component {...props} />;
          }

          return (
            <Redirect to={{ pathname, state: { from: props.location } }} />
          );
        }}
      />
    );
  };
}

export const PrivateRoute = withAuthRoute(
  () => checkAuthentication(),
  '/login'
);

export const PublicOnlyRoute = withAuthRoute(
  () => !checkAuthentication(),
  '/home'
);
