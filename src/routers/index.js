import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { PublicOnlyRoute, PrivateRoute } from './authenRoute'
import Login from '../pages/login'
import Welcome from '../pages/welcome/welcomePage.js'

const Routers = () => {
  return (
    <div>
    <Switch>
      <PrivateRoute path="/home" component={Welcome} />
      <PublicOnlyRoute path="/login" component={Login} />
      <Route render={() => <>404</>} />
    </Switch>
    </div>
  )
}
export default Routers